package com.moveassist.dbanonymise.task;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.sql.DataSource;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.List;
import java.util.Map;

@Component
public class EncryptionProcessor {

    private static final Logger log = LoggerFactory.getLogger(EncryptionProcessor.class);
    private final JdbcTemplate jdbcTemplate;

    private static final String ALGORITHM = "AES/CBC/PKCS5PADDING";
    private static final int IV_SIZE = 16;
    private static final int KEY_SIZE = 16;

    private static String encryptionKey = "";

    public EncryptionProcessor(final DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public void process(Map<String, String> anonymizePIIFields, String encryptionKey) {
        log.info("Encrypting data...");
        this.encryptionKey = encryptionKey;
        for (Map.Entry<String, String> entry : anonymizePIIFields.entrySet()) {
            encryptTable(entry.getKey(), entry.getValue());
        }
        log.info("DONE encrypting database..");
    }

    private void encryptTable(String tableName, String tableColumns) {
        long from = System.currentTimeMillis();
        List<Map<String, Object>> tableRecords = this.jdbcTemplate.queryForList("SELECT id, " + tableColumns + " FROM " + tableName);
        final String updateStatement = createUpdateStatement(tableName, tableColumns);
        log.info(updateStatement);
        for (Map<String, Object> map : tableRecords) {
            if ("Address".equals(tableName)) {
                this.jdbcTemplate.update(updateStatement, encryptData(map.get("address")), encryptData(map.get("street")),
                        encryptData(map.get("town")), encryptData(map.get("state")), encryptData(map.get("county")),
                        encryptData(map.get("postalCode")), encryptData(map.get("countryCode")),
                        encryptData(map.get("countryName")), map.get("id"));
            } else if ("Contact".equals(tableName)) {
                this.jdbcTemplate.update(updateStatement, encryptData(map.get("employeeNumber")), encryptData(map.get("initials")),
                        encryptData(map.get("title")), encryptData(map.get("firstName")), encryptData(map.get("lastName")),
                        encryptData(map.get("middleName")), encryptData(map.get("nickName")), encryptData(map.get("jobTitle")),
                        encryptData(map.get("addressAs")), encryptData(map.get("contactNationalityCode")),
                        encryptData(map.get("contactNationalityName")), encryptData(map.get("contactTypeCode")),
                        encryptData(map.get("contactTypeName")), map.get("id"));
            } else if ("ContactDetails".equals(tableName)) {
                this.jdbcTemplate.update(updateStatement, encryptData(map.get("phone")), encryptData(map.get("phone2")),
                        encryptData(map.get("mobilePhone")), encryptData(map.get("fax")), encryptData(map.get("privateFax")),
                        encryptData(map.get("email")), encryptData(map.get("email2")), encryptData(map.get("privateEmail")),
                        encryptData(map.get("externalEmail")), encryptData(map.get("contactName")), encryptData(map.get("skypeID")), map.get("id"));
            } else if ("Relocatee".equals(tableName)) {
                try {
                    this.jdbcTemplate.update(updateStatement, encryptData(map.get("employeeNumber")), encryptData(map.get("gradeSimple")),
                            encryptData(map.get("socialSecurityNumber")), encryptData(map.get("passportNumber")), encryptData(map.get("passportIssuedAt")),
                            encryptData(map.get("occupation")), encryptData(map.get("typeOfSchool")), encryptData(map.get("otherNationality")),
                            encryptData(map.get("schoolGrade")), encryptData(map.get("currentJobTitle")), encryptData(map.get("localIDCardNo")),
                            encryptData(map.get("newJobTitle")), encryptData(map.get("specialNeeds")), encryptData(map.get("townOfBirth")),
                            encryptData(map.get("nextOfKin")), encryptData(map.get("emergencyContactDetails")), encryptData(map.get("emailAddress")),
                            encryptData(map.get("bank")), encryptData(map.get("branch")), encryptData(map.get("sortCode")),
                            encryptData(map.get("accountNumber")), encryptData(map.get("accountName")),
                            encryptData(map.get("employeeClassCode")), encryptData(map.get("employeeClassName")), encryptData(map.get("divisionCode")),
                            encryptData(map.get("divisionName")), encryptData(map.get("gradeCode")), encryptData(map.get("gradeName")),
                            encryptData(map.get("ethnicityCode")), encryptData(map.get("ethnicityName")), encryptData(map.get("countryOfBirthCode")),
                            encryptData(map.get("countryOfBirthName")), encryptData(map.get("passportCountryCode")), encryptData(map.get("passportCountryName")),
                            encryptData(map.get("genderCode")), encryptData(map.get("genderName")), encryptData(map.get("maritalStatusCode")),
                            encryptData(map.get("maritalStatusName")), encryptData(map.get("nationalityCode")), encryptData(map.get("nationalityName")),
                            encryptData(map.get("dateOfBirth")), encryptData(map.get("passportIssueDate")), encryptData(map.get("passportExpiry")),
                            encryptData(map.get("familySize")), encryptData(map.get("workPermit")), map.get("id"));
                } catch (Exception e) {
                    // in case there is no employeeNumber column in Relocatee table
                    this.jdbcTemplate.update(updateStatement, encryptData(map.get("gradeSimple")), encryptData(map.get("socialSecurityNumber")),
                            encryptData(map.get("passportNumber")), encryptData(map.get("passportIssuedAt")), encryptData(map.get("occupation")),
                            encryptData(map.get("typeOfSchool")), encryptData(map.get("otherNationality")), encryptData(map.get("schoolGrade")),
                            encryptData(map.get("currentJobTitle")), encryptData(map.get("localIDCardNo")), encryptData(map.get("newJobTitle")),
                            encryptData(map.get("specialNeeds")), encryptData(map.get("townOfBirth")), encryptData(map.get("nextOfKin")),
                            encryptData(map.get("emergencyContactDetails")), encryptData(map.get("emailAddress")), encryptData(map.get("bank")),
                            encryptData(map.get("branch")), encryptData(map.get("sortCode")), encryptData(map.get("accountNumber")),
                            encryptData(map.get("accountName")), encryptData(map.get("employeeClassCode")), encryptData(map.get("employeeClassName")),
                            encryptData(map.get("divisionCode")), encryptData(map.get("divisionName")), encryptData(map.get("gradeCode")),
                            encryptData(map.get("gradeName")), encryptData(map.get("ethnicityCode")), encryptData(map.get("ethnicityName")),
                            encryptData(map.get("countryOfBirthCode")), encryptData(map.get("countryOfBirthName")),
                            encryptData(map.get("passportCountryCode")), encryptData(map.get("passportCountryName")),
                            encryptData(map.get("genderCode")), encryptData(map.get("genderName")), encryptData(map.get("maritalStatusCode")),
                            encryptData(map.get("maritalStatusName")), encryptData(map.get("nationalityCode")), encryptData(map.get("nationalityName")),
                            encryptData(map.get("dateOfBirth")), encryptData(map.get("passportIssueDate")), encryptData(map.get("passportExpiry")),
                            encryptData(map.get("familySize")), encryptData(map.get("workPermit")), map.get("id"));
                }
            } else if ("RAUser".equals(tableName)) {
                this.jdbcTemplate.update(updateStatement, encryptData(map.get("userLogin")), map.get("id"));
            } else if ("Job".equals(tableName)) {
                this.jdbcTemplate.update(updateStatement, encryptData(map.get("initiationFormJson")), map.get("id"));
            } else if ("SVCVisa".equals(tableName)) {
                this.jdbcTemplate.update(updateStatement, encryptData(map.get("validPeriod")), encryptData(map.get("visaNumber")),
                        encryptData(map.get("visaExpiryDate")), encryptData(map.get("validFromDate")), map.get("id"));
            } else if ("SVCVisaCancellation".equals(tableName)) {
                this.jdbcTemplate.update(updateStatement, encryptData(map.get("visaNumber")), encryptData(map.get("visaExpiryDate")), map.get("id"));
            } else if ("VisaDocument".equals(tableName)) {
                this.jdbcTemplate.update(updateStatement, encryptData(map.get("validPeriod")), encryptData(map.get("visaNumber")),
                        encryptData(map.get("visaExpiryDate")), encryptData(map.get("validFromDate")), map.get("id"));
            } else if ("SVCPet".equals(tableName)) {
                this.jdbcTemplate.update(updateStatement, encryptData(map.get("animalType")), encryptData(map.get("name")), encryptData(map.get("age")),
                        encryptData(map.get("petPassport")), encryptData(map.get("breed")), encryptData(map.get("specialRequirements")), map.get("id"));
            } else if ("SVCCrossCultural".equals(tableName)) {
                this.jdbcTemplate.update(updateStatement, encryptData(map.get("serviceFor")), map.get("id"));
            } else if ("SVCLanguageTraining".equals(tableName)) {
                this.jdbcTemplate.update(updateStatement, encryptData(map.get("serviceFor")), map.get("id"));
            } else if ("SVCTempAcc".equals(tableName)) {
                this.jdbcTemplate.update(updateStatement, encryptData(map.get("petDetails")), map.get("id"));
            } else if ("SVCDriverLicence".equals(tableName)) {
                this.jdbcTemplate.update(updateStatement, encryptData(map.get("licenceNumber")), encryptData(map.get("licenceExpiryDate")), map.get("id"));
            }
        }
        log.info("Encrypting " + tableName + " TOOK " + (System.currentTimeMillis() - from) + " milliseconds.");
    }

    private static String createUpdateStatement(String tableName, String tableColumns) {
        String[] columns = tableColumns.split(",");
        String update = "UPDATE " + tableName + " SET ";
        for (String column : columns) {
            update += column + " = ?,";
        }
        update = update.substring(0, update.lastIndexOf(","));
        update += " WHERE id = ?";
        return update;
    }

    private static String encryptData(Object object) {
        if (object != null) {
            if (object != "") {
                return encryptAesCbc((String) object);
            }
        }
        return null;
    }

    /**
     * Encrypt String using AES/CBC/PKCS5PADDING.
     * @param value the plain text value to encrypt
     * @return the encrypted value
     */
    public static String encryptAesCbc(String value) {
        return encryptAesCbc(encryptionKey, value);
    }

    /**
     * Encrypt String using AES/CBC/PKCS5PADDING.
     * @param key the encryption key
     * @param value the plain text value to encrypt
     * @return the encrypted value
     */
    private static String encryptAesCbc(String key, String value) {
        try {
            final byte[] iv = new byte[IV_SIZE];
            final SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
            random.nextBytes(iv);
            final IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

            final MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.update(key.getBytes("UTF-8"));
            final byte[] keyBytes = new byte[KEY_SIZE];
            System.arraycopy(digest.digest(), 0, keyBytes, 0, keyBytes.length);
            final SecretKeySpec skeySpec = new SecretKeySpec(keyBytes, "AES");

            final Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivParameterSpec);

            final byte[] encrypted = cipher.doFinal(value.getBytes());
            final byte[] ivAndEncryptedText = new byte[IV_SIZE + encrypted.length];
            System.arraycopy(iv, 0, ivAndEncryptedText, 0, IV_SIZE);
            System.arraycopy(encrypted, 0, ivAndEncryptedText, IV_SIZE, encrypted.length);

            return Base64.encodeBase64String(ivAndEncryptedText).trim();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

}
