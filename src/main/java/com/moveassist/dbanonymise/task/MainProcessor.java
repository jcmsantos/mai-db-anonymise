package com.moveassist.dbanonymise.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class MainProcessor {

    private static final Logger log = LoggerFactory.getLogger(MainProcessor.class);

    private final String encryptionKey;
    private final boolean encryptionEnabled;
    private final boolean bypassLocations;
    private final String bypassRAUserIds;
    private final boolean anonymisationEnabled;
    private final AnonymisationProcessor anonymisationProcessor;
    private final EncryptionProcessor encryptionProcessor;

    private static Map<String, String> commonPIIFields; // both anonymise and encrypt
    private static Map<String, String> encryptPIIFields; // both anonymise and encrypt
    private static Map<String, String> nullablePIIFields; // anonymise only
    private static Map<String, String> anonymizePIIFields;  // anonymise only

    @Autowired
    public MainProcessor(
            @Value("${anonymisation.enabled:false}") final boolean anonymisationEnabled,
            @Value("${anonymisation.bypassLocations:false}") final boolean bypassLocations,
            @Value("${anonymisation.bypassRAUserIds:}") final String bypassRAUserIds,
            @Value("${encryption.enabled:false}") final boolean encryptionEnabled,
            @Value("${encryption.key:}") final String encryptionKey,
            final AnonymisationProcessor anonymisationProcessor,
            final EncryptionProcessor encryptionProcessor) {
        this.anonymisationEnabled = anonymisationEnabled;
        this.bypassLocations = bypassLocations;
        this.bypassRAUserIds = bypassRAUserIds;
        this.encryptionEnabled = encryptionEnabled;
        this.encryptionKey = encryptionKey;
        this.anonymisationProcessor = anonymisationProcessor;
        this.encryptionProcessor = encryptionProcessor;
    }

    public void process() {
        initializeCommonPIIFields();
        initializeEncryptPIIFields();

        if (anonymisationEnabled) {
            HashMap<String, String> fieldsToAnonymise = new HashMap<>();
            HashMap<String, String> fieldsToNull = new HashMap<>();
            initializeAnonymisePIIFields(bypassLocations);
            initializeNullablePIIFields(bypassLocations);
            appendMapValues(fieldsToAnonymise, commonPIIFields);
            appendMapValues(fieldsToNull, encryptPIIFields);
            appendMapValues(fieldsToAnonymise, anonymizePIIFields);
            appendMapValues(fieldsToNull, nullablePIIFields);
            anonymisationProcessor.process(fieldsToAnonymise, fieldsToNull, bypassRAUserIds);
        }
        if (encryptionEnabled) {
            if (!"".equals(encryptionKey.trim())) {
                encryptPIIFields.put("Address", "countryCode, countryName");
                HashMap<String, String> fieldsToEncrypt = new HashMap<>();
                appendMapValues(fieldsToEncrypt, commonPIIFields);
                appendMapValues(fieldsToEncrypt, encryptPIIFields);
                encryptionProcessor.process(fieldsToEncrypt, this.encryptionKey);
            } else {
                log.info("Cannot find encryption.key from application.properties. Data encryption is disabled.");
            }
        }
    }

    // string fields that will be anonymise and/or encrypt
    private static void initializeCommonPIIFields() {
        commonPIIFields = new HashMap<>();
        commonPIIFields.put("Address", "address, street, town, state, county, postalCode");
        commonPIIFields.put("Contact", "employeeNumber, initials, title, firstName, lastName, middleName, nickName, " +
                "jobTitle, addressAs");
        commonPIIFields.put("ContactDetails", "phone, phone2, mobilePhone, fax, privateFax, email, email2, privateEmail, " +
                "externalEmail, contactName, skypeID");
        commonPIIFields.put("Job", "initiationFormJson");
        commonPIIFields.put("Relocatee", "employeeNumber, gradeSimple, socialSecurityNumber, passportNumber, passportIssuedAt, " +
                "occupation, typeOfSchool, otherNationality, schoolGrade, currentJobTitle, localIDCardNo, newJobTitle, " +
                "specialNeeds, townOfBirth, nextOfKin, emergencyContactDetails, emailAddress, bank, branch, sortCode, " +
                "accountNumber, accountName");
        commonPIIFields.put("SVCCrossCultural", "serviceFor");
        commonPIIFields.put("SVCDriverLicence", "licenceNumber");
        commonPIIFields.put("SVCLanguageTraining", "serviceFor");
        commonPIIFields.put("SVCPet", "animalType, name, age, petPassport, breed, specialRequirements");
        commonPIIFields.put("SVCTempAcc", "petDetails");
        commonPIIFields.put("SVCVisa", "validPeriod, visaNumber");
        commonPIIFields.put("SVCVisaCancellation", "visaNumber");
        commonPIIFields.put("VisaDocument", "validPeriod, visaNumber");
    }

    // country, systemoption or other fields that will be set to null when anonymise and/or encrypt
    private static void initializeEncryptPIIFields() {
        encryptPIIFields = new HashMap<>();
        encryptPIIFields.put("Contact", "contactNationalityCode, contactNationalityName, contactTypeCode, contactTypeName");
        encryptPIIFields.put("RAUser", "userLogin");
        encryptPIIFields.put("Relocatee", "employeeClassCode, employeeClassName, divisionCode, divisionName, gradeCode, " +
                "gradeName, ethnicityCode, ethnicityName, countryOfBirthCode, countryOfBirthName, passportCountryCode, " +
                "passportCountryName, genderCode, genderName, maritalStatusCode, maritalStatusName, nationalityCode, " +
                "nationalityName, dateOfBirth, passportIssueDate, passportExpiry, familySize, workPermit");
        encryptPIIFields.put("SVCDriverLicence", "licenceExpiryDate ");
        encryptPIIFields.put("SVCVisa", "visaExpiryDate, validFromDate");
        encryptPIIFields.put("SVCVisaCancellation", "visaExpiryDate");
        encryptPIIFields.put("VisaDocument", "visaExpiryDate, validFromDate");
    }

    // fields that will be set to null when anonymise
    private static void initializeNullablePIIFields(boolean bypassLocations) {
        nullablePIIFields = new HashMap<>();
        nullablePIIFields.put("Contact", "languageCode, languageType, contactNationality");
        nullablePIIFields.put("ContactDetails", "phoneTypeCode, phoneTypeType, phone2TypeCode, phone2TypeType");
        nullablePIIFields.put("Job", "benefitPlanCode, benefitPlanType, benefitPlanEndDate"
                + (!bypassLocations ? ", fromLocation, toLocation, homeCountry, hostCountry" : ""));
        nullablePIIFields.put("RecurringService", "billTo");
        nullablePIIFields.put("Relocatee", "lengthOfAssignment, misc01Code, misc01Type, misc02Code, misc02Type, misc03Code, " +
                "misc03Type, misc04Code, misc04Type, misc05Code, misc05Type, liableCostCenterCode, liableCostCenterType, " +
                "terminationDate, departureDate, arrivalDate, workStartDate, NICStartDate, baseSalaryCurrency, " +
                "originCorporateAccount, newCorporateAccount, relocateeTypeCode, relocateeTypeType, nationality, " +
                "passportCountry, countryOfBirth");
        nullablePIIFields.put("UserDefinedValue", "fieldValue");
        if (!bypassLocations) {
            nullablePIIFields.put("Address", "country, countryCode, countryName");
            nullablePIIFields.put("JobService", "serviceLocation");
            nullablePIIFields.put("JobServicePack", "fromLocation, toLocation");
            nullablePIIFields.put("SVCResidence", "fromLocation, toLocation");
        }
    }

    // fields that will be anonymise only
    private static void initializeAnonymisePIIFields(boolean bypassLocations) {
        anonymizePIIFields = new HashMap<>();
        anonymizePIIFields.put("Address", "GBLOC, notes");
        anonymizePIIFields.put("Attachment", "fileName, originalName");
        anonymizePIIFields.put("Contact", "note");
        anonymizePIIFields.put("Correspondence", "sendToEmail, ccToEmail, bccToEmail, emailSubject, text");
        anonymizePIIFields.put("Debtor", "name");
        anonymizePIIFields.put("FurnitureItem", "warrantyDetails");
        anonymizePIIFields.put("InsuranceClaimItem", "notes");
        anonymizePIIFields.put("InsuranceClaimNote", "details");
        anonymizePIIFields.put("JobComplaint", "description, notes, prevention");
        anonymizePIIFields.put("JobQualityRating", "comments");
        anonymizePIIFields.put("JobService", "comments");
        anonymizePIIFields.put("JobTransaction", "notes");
        anonymizePIIFields.put("LeaseCommission", "notes");
        anonymizePIIFields.put("LeaseDeposit", "notes");
        anonymizePIIFields.put("LeaseUtility", "notes");
        anonymizePIIFields.put("Note", "text");
        anonymizePIIFields.put("RecurringService", "invoiceNotes");
        anonymizePIIFields.put("Relocatee", "otherLanguages, comments, liableCostCenterText, costCenter, externalID1, " +
                "externalID2, petDetails, baseSalary, interests, alertNotes, religion, otherReference, payrollNumber");
        anonymizePIIFields.put("SVCAirportPickup", "destination, phone");
        anonymizePIIFields.put("SVCAuto", "registration");
        anonymizePIIFields.put("SVCCheckIn", "preInspectionReport, exceptionNotes");
        anonymizePIIFields.put("SVCCheckOut", "preInspectionReport, exceptionNotes, movingTo");
        anonymizePIIFields.put("SVCDeparture", "movingTo");
        anonymizePIIFields.put("SVCFurnitureRental", "deliveryComments, collectionComments");
        anonymizePIIFields.put("SVCHomeSearch", "hssNotes, statusNotes");
        anonymizePIIFields.put("SVCLeaseClosedown", "TVNotes, phoneNotes, conditionReport");
        anonymizePIIFields.put("SVCLeaseNegotiation", "legalRepairResponsibilities, breakNotes, dipClauseNotes");
        anonymizePIIFields.put("SVCResidence", "accessDetailsNotes");
        anonymizePIIFields.put("SVCSurvey", "allowanceDescription, destinationAccess, surveyDescription, originAccess, " +
                "destinationAccess");
        anonymizePIIFields.put("SVCStorage", "notes, suspendedNotes, completeNotes");

        // need to add empty field for the nullable fields of the table
        if (!bypassLocations) {
            anonymizePIIFields.put("JobServicePack", "");
        }
        anonymizePIIFields.put("RAUser", "");
        anonymizePIIFields.put("UserDefinedValue", "");
    }

    private static void appendMapValues(Map<String, String> map1, Map<String, String> map2) {
        for (Map.Entry<String, String> entry : map2.entrySet()) {
            if (map1.get(entry.getKey()) == null) {
                map1.put(entry.getKey(), entry.getValue());
            } else {
                map1.put(entry.getKey(), map1.get(entry.getKey()) + ", " + entry.getValue());
            }
        }
    }
}
