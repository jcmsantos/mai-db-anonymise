package com.moveassist.dbanonymise.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;

@Component
public class AnonymisationProcessor {

    private static final Logger log = LoggerFactory.getLogger(AnonymisationProcessor.class);
    private static final String anonymizedString = "xxxxxxxx";
    private final JdbcTemplate jdbcTemplate;

    public AnonymisationProcessor(final DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public void process(Map<String, String> anonymizePIIFields, Map<String, String> nullifyPIIFields, String recordIds) {
        log.info("Anonymising data...");
        for (Map.Entry<String, String> entry : anonymizePIIFields.entrySet()) {
            String tableName = entry.getKey();
            log.info("Anonymising " + tableName + "..");
            String updateStatement = "UPDATE " + tableName + ("Debtor".equals(tableName) ? " d INNER JOIN Party p ON p.id = d.id" : "");
            if (!"".equals(entry.getValue())) {
                updateStatement += " SET " + entry.getValue().replaceAll(",", " = \"" + anonymizedString + "\",")
                        + " = \"" + anonymizedString + "\"";
                if (nullifyPIIFields.get(tableName) != null) {
                    updateStatement += ", " + nullifyPIIFields.get(tableName).replaceAll(",", " = NULL,") + " = NULL";
                }
            } else if (nullifyPIIFields.get(tableName) != null) {
                updateStatement += " SET " + nullifyPIIFields.get(tableName).replaceAll(",", " = NULL,") + " = NULL";
            }

            if ("Attachment".equals(tableName)) {
                updateStatement += ", deleted = true";
            } else if ("JobService".equals(tableName)) {
                updateStatement += ", description = CONCAT(serviceType, \"_\", id)";
            } else if ("Debtor".equals(tableName)) {
                updateStatement += " WHERE debtorTypeCode = \"PRIVATE\"";
                String privateDebtorCodes = getPrivateDebtorCodes();
                if (!"".equals(privateDebtorCodes)) {
                    updateStatement += " OR code IN (" + privateDebtorCodes +")";
                }
            }

            if (recordIds != null && !"".equals(recordIds)) {
                if ("RAUser".equals(tableName) || "Contact".equals(tableName) || "Relocatee".equals(tableName)) {
                    updateStatement += " WHERE id NOT IN (" + recordIds + ")";
                } else if ("ContactDetails".equals(tableName)) {
                    String contactDetailIds = "";
                    List<String> contactDetailsList = this.jdbcTemplate
                            .queryForList("SELECT contactDetails FROM Contact WHERE contactDetails IS NOT NULL AND id IN (" + recordIds + ")", String.class);
                    for (String id : contactDetailsList) {
                        contactDetailIds += id + ",";
                    }
                    if (contactDetailIds != "") {
                        contactDetailIds = contactDetailIds.substring(0, contactDetailIds.lastIndexOf(","));
                        updateStatement += " WHERE id NOT IN (" + contactDetailIds + ")";
                    }
                }
            }
            log.info(updateStatement);
            updateTable(anonymizePIIFields, tableName, updateStatement);
        }
        log.info("DONE anonymising tables..");
    }

    private void updateTable(Map<String, String> anonymizePIIFields, String tableName, String updateStatement) {
        try {
            this.jdbcTemplate.update(updateStatement);
        } catch (Exception e) {
            String errorMessage = e.getLocalizedMessage();
            int startIndex = errorMessage.indexOf("Unknown column '");
            int endIndex = errorMessage.indexOf("' in 'field list'");
            if (startIndex != -1 && endIndex != -1) {
                String columnName = errorMessage.substring(startIndex + 16, endIndex);
                String anonymizeColumns = anonymizePIIFields.get(tableName);
                if (anonymizeColumns != null && (anonymizeColumns.startsWith(columnName)
                        || anonymizeColumns.contains(columnName + ",") || anonymizeColumns.endsWith(columnName))) {
                    String columnUpdate = " " + columnName + " = \"" + anonymizedString + "\",";
                    if (updateStatement.indexOf(columnUpdate) != -1) {
                        updateStatement = updateStatement.replaceFirst(columnUpdate, "");
                        log.warn("Removed column " + columnName + " : " + updateStatement);
                        updateTable(anonymizePIIFields, tableName, updateStatement);
                    } else {
                        columnUpdate = ", " + columnName + " = \"" + anonymizedString + "\"";
                        if (updateStatement.indexOf(columnUpdate) != -1) {
                            updateStatement = updateStatement.replaceFirst(columnUpdate, "");
                            log.warn("Removed column " + columnName + " : " + updateStatement);
                            updateTable(anonymizePIIFields, tableName, updateStatement);
                        } else {
                            log.error("Can't find column " + columnName);
                        }
                    }
                } else {
                    String columnUpdate = " " + columnName + " = NULL,";
                    if (updateStatement.indexOf(columnUpdate) != -1) {
                        updateStatement = updateStatement.replaceFirst(columnUpdate, "");
                        log.warn("Removed column " + columnName + " : " + updateStatement);
                        updateTable(anonymizePIIFields, tableName, updateStatement);
                    } else {
                        columnUpdate = ", " + columnName + " = NULL";
                        if (updateStatement.indexOf(columnUpdate) != -1) {
                            updateStatement = updateStatement.replaceFirst(columnUpdate, "");
                            log.warn("Removed column " + columnName + " : " + updateStatement);
                            updateTable(anonymizePIIFields, tableName, updateStatement);
                        } else {
                            log.error("Can't find column " + columnName);
                        }
                    }
                }
            } else {
                endIndex = errorMessage.indexOf("' in 'where clause'");
                if (endIndex != -1) {
                    String columnName = errorMessage.substring(startIndex + 16, endIndex);
                    if ("debtorTypeCode".equals(columnName)) {
                        updateStatement = updateStatement.replaceFirst(" debtorTypeCode = \"PRIVATE\"", "");
                        log.warn("Removed column " + columnName + " in where clause : " + updateStatement);
                        updateTable(anonymizePIIFields, tableName, updateStatement);
                    }
                } else {
                    log.error(e.getLocalizedMessage(), e);
                }
            }
        }
    }

    private String getPrivateDebtorCodes() {
        String privateDebtorCodes = "";
        List<String> assigneeDebtorCode = this.jdbcTemplate
                .queryForList("SELECT valueAlpha FROM SystemOption WHERE code = \"ASSIGNEE_DEBTOR_CODE\" AND type = \"PARAMETER\"", String.class);
        privateDebtorCodes = appendDebtorCode(privateDebtorCodes, assigneeDebtorCode);

        List<String> genericPrivateDebtorCodes = this.jdbcTemplate
                .queryForList("SELECT valueAlpha FROM SystemOption WHERE code = \"GENERIC_PRIVATE_DEBTOR_CODES\" AND type = \"PARAMETER\"", String.class);
        privateDebtorCodes = appendDebtorCode(privateDebtorCodes, genericPrivateDebtorCodes);

        List<String> assigneeDebtorCodeOffices = this.jdbcTemplate
                .queryForList("SELECT valueAlpha FROM OfficeSystemOption WHERE code = \"ASSIGNEE_DEBTOR_CODE\" AND type = \"PARAMETER\"", String.class);
        privateDebtorCodes = appendDebtorCode(privateDebtorCodes, assigneeDebtorCodeOffices);

        List<String> genericPrivateDebtorCodesOffices = this.jdbcTemplate
                .queryForList("SELECT valueAlpha FROM OfficeSystemOption WHERE code = \"GENERIC_PRIVATE_DEBTOR_CODES\" AND type = \"PARAMETER\"", String.class);
        privateDebtorCodes = appendDebtorCode(privateDebtorCodes, genericPrivateDebtorCodesOffices);

        int lastCommaIndex = privateDebtorCodes.lastIndexOf(",");
        if (lastCommaIndex > 0) {
            privateDebtorCodes = privateDebtorCodes.substring(0, lastCommaIndex);
        }
        return privateDebtorCodes;
    }

    private String appendDebtorCode(String privateDebtorCodes, List<String> debtorCodes) {
        for (String debtorCode : debtorCodes) {
            if (debtorCode != null && !"".equals(debtorCode)) {
                String[] debtorCodeArray = debtorCode.split(",");
                for (String code : debtorCodeArray) {
                    code = "\"" + code.trim() + "\",";
                    if (!privateDebtorCodes.contains(code)) {
                        privateDebtorCodes += code;
                    }
                }
            }
        }
        return privateDebtorCodes;
    }

}
