package com.moveassist.dbanonymise;

import com.moveassist.dbanonymise.task.MainProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class Application implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    private final MainProcessor mainProcessor;

    public Application(final MainProcessor mainProcessor) {
        this.mainProcessor = mainProcessor;
    }

    public static void main(String[] args) {
        new SpringApplicationBuilder(Application.class).logStartupInfo(false).run(args);
    }

    @Override
    public void run(String... args) {
        log.info("Running db anonymisation and encryption...");
        mainProcessor.process();
    }

}